WEBSOCKET

- install broadcasting -> masuk tab broadcasting | install laravel reverb -> yes | yes
- php artisan make:event <nama_event>
- pada judul event tambahkan - implement ShouldBroadcast
- broadcastOn rubah dari PrivateChannel to Channel
- tambahkan public function broadcastWith

NOTE
privateChannel : bisa pilih mau dikirim ke user id mana
Channel : akan dikirim ke semua

[PRIVATE CHANNEL]
construct($data, $user_id);
pada broadcastOn tambahkan variable user_id -> new PrivateChannel('private-channel.user.' . $this->userid);

tambahkan pada channels.php
Broadcast::channel('private-channel.user.{id}', function ($user, $id){
    return $user->id == $id;
})

